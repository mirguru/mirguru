import { withPluginApi } from 'discourse/lib/plugin-api';

export default {
  name: 'header-menu',
  initialize(){
   

    withPluginApi('0.4', api => {

    api.decorateWidget('home-logo:after', helper => {
    const showExtraInfo = helper.attrs.minimized;
        if(!showExtraInfo) {
            return helper.h('div#primary-top', [
                helper.h('div#primary-top', [
                    helper.h('ul', [
                        helper.h('li', [
                            helper.h('a', {
                                href:'/',
                                text:'Вопросы и ответы'
                            }),
                        ]),
                        helper.h('li', [
                            helper.h('a', {
                                href:'/categories',
                                text:'Страны'
                            })
                        ]),
                        helper.h('li', [
                            helper.h('a', {
                                href:'/tags',
                                text:'Рубрики'
                            })
                        ])

                    ])
                ])
            ])
        }

      })
      
    });
    
  }
}