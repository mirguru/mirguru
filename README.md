# Mir.Guru customization
A Discourse plugin for a custom project.

## Installation

To install using docker, add the following to your app.yml in the plugins section:

```
hooks:
  after_code:
    - exec:
        cd: $home/plugins
        cmd:
          - mkdir -p plugins
          - git clone https://mirguru@bitbucket.org/mirguru/mirguru.git
```

and rebuild docker via

```
cd /var/discourse
./launcher rebuild app
```

## Settings

You can override the extra info and have the search input display instead by checking the setting ``override extra info`` in Settings > Plugins. 