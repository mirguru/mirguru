# name: discourse-mirguru
# about: Discourse customizations for Mir.Guru
# version: 1.8
# authors: Anastasia

register_asset 'stylesheets/spices.scss', :desktop

after_initialize do
  ApplicationController.class_eval do
	def set_layout
  	File.expand_path('../views/layouts/custom.html.erb', __FILE__)
	end

    private

  end

end